#!/usr/bin/python3
import flowchart_item
import sys
import gnome


# TODO: Make UI
# TODO: Add Forms

# FLOWCHART_TYPES = ("start",)


def main():
    start_type = flowchart_item.FlowChartType("start", 0, 1)
    end_type = flowchart_item.FlowChartType("end", 1, 0)
    define_type = flowchart_item.FlowChartType("define", 1, 1, "[variable_name] = [default_value]")
    print_type = flowchart_item.FlowChartType("print", 1, 1, "print([string])")
    assign_type = flowchart_item.FlowChartType("assign", 1, 1, "[variable_name] = [new_value]")
    if_type = flowchart_item.FlowChartType("if", 1, 2, "if [condition]:")
    types = [start_type, end_type, define_type, print_type, assign_type, if_type]
    app = gnome.Application(types, application_id="com.flow.FlowChartApp")
    error = app.run(sys.argv)
    sys.exit(error)


if __name__ == "__main__":
    main()
