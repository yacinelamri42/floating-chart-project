from flowchart_item import FlowChartType, FlowChartItem, Program
import pprint

start_type = FlowChartType("start", 0, 1)
end_type = FlowChartType("end", 1, 0)
define_type = FlowChartType("define", 1, 1, "[variable_name] = [default_value]")
print_type = FlowChartType("print", 1, 1, "print([string])")
assign_type = FlowChartType("assign", 1, 1, "[variable_name] = [new_value]")
if_type = FlowChartType("if", 1, 2, "if [condition]:")

# Creating type_file
# type_list = [start_type, end_type, print_type, assign_type, if_type]
# export_types(type_list)
#
# start = FlowChartItem("start", start_type)
# end1 = FlowChartItem("end1", end_type)
# end2 = FlowChartItem("end2", end_type)
# define_a = FlowChartItem("define a", define_type, {"variable_name": "a",
#                                                    "default_value": 5})
# print_a = FlowChartItem("print a", print_type, {"string": "a"})
# print_err = FlowChartItem("print err", print_type, {"string": "'Error'"})
# if_item = FlowChartItem("if item", if_type, {"condition": "a == 5"})

start = start_type.create_item({"variable_name": "a", "default_value": 5})
end1 = end_type.create_item()
end2 = end_type.create_item()
define_a = define_type.create_item({"variable_name": "a", "default_value": 5})
print_a = print_type.create_item({"string": "a"})
print_err = print_type.create_item({"string": "'Error'"})
if_item = if_type.create_item({"condition": "a == 5"})


start.add_to_program_queue(define_a)
define_a.add_to_program_queue(if_item)
if_item.add_to_program_queue(print_a)
if_item.add_to_program_queue(print_err)
print_a.add_to_program_queue(end1)
print_err.add_to_program_queue(end2)

program = Program("test", start)
program.read_program(program.start_item)
pp = pprint.PrettyPrinter(indent=4)
program.generate_code()
program.print_program()
print(program.python_code)
pp.pprint(program.depth)
