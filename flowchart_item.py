import constants
import re


class FlowChartType:
    def __init__(
            self, name="", num_inputs=-1, num_outputs=-1,
            format="", image_file=""
            ):
        if name != "" and num_inputs > -1 and num_outputs > -1:
            self.action_type = name
            self.number_inputs = num_inputs
            self.number_outputs = num_outputs
            self.format = format
            self.arguments = [item.strip('[]') for item in re.findall(r'\[.*?\]', format)]
        self.num_items = 0
        self.items = []
        self.image_file = image_file


    def __str__(self):
        return f"{self.action_type}:{self.number_inputs}:{self.number_outputs}"

    def __dict__(self):
        return {"action_type": self.action_type,
                "number_inputs": self.number_inputs,
                "number_outputs": self.number_outputs,
                "format": self.format}
    
    def create_item(self, args: dict={}):
        self.num_items+=1
        name = self.action_type + str(self.num_items)
        if not list(args.keys()) == self.arguments:
            args = dict.fromkeys(self.arguments)
        return FlowChartItem(name, self, args)

class FlowChartItem:
    def __init__(self, name, action_type: FlowChartType, args: dict = {}):
        self.name = name
        self.item_type = action_type
        self.next = []
        self.previous = []
        if list(args.keys()) == self.item_type.arguments:
            self.arguments = args
        else:
            print(args.keys())
            print(self.item_type.arguments)
            raise Exception("Cannot use arguments given since it doesnt fit")

    def __str__(self):
        return f"{self.name}:{self.item_type}"

    def add_to_program_queue(self, next_item: "FlowChartItem"):
        # using quotes for FlowChartItem type because python needs it
        if len(self.next) == self.item_type.number_outputs:
            raise Exception("Cannot add more outputs")
        if len(next_item.previous) == next_item.item_type.number_inputs:
            raise Exception("Cannot add more inputs")
        self.next.append(next_item)
        next_item.previous.append(self)

    def remove_program_from_queue(self):
        for item in self.next:
            item.previous.remove(self)
        for item in self.previous:
            item.next.remove(self)
        self.previous = []
        self.next = []

    def replace_item_in_program_queue(self, old_item: "FlowChartItem"):
        # using quotes for FlowChartItem type because python needs it
        self.remove_program_from_queue()
        next = old_item.next
        previous = old_item.previous
        old_item.remove_program_from_queue()
        for item in next:
            item.add_to_program_queue(self, item)
        for item in previous:
            item.add_to_program_queue(item, self)


class Program:
    def __init__(self, name, start_item: FlowChartItem):
        self.name = name
        self.start_item = start_item
        self.program = []
        self.branches = [[self.start_item]]
        self.depth = [1]
        self.total_branches = 0
        self.python_code = ""

    def read_program(self, item: FlowChartItem, current_branch=0):
        next_items = item.next
        if len(next_items) == 0:
            if not item.item_type.number_outputs == 0:
                raise Exception(f"({item}):({current_branch}) needs output, received none")
            return
        branching_depth = 0
        if len(next_items) > 1:
            branching_depth = self.depth[0]
        for index in range(len(next_items)):
            if not index == 0:
                self.branches.append([])
                for i in range(branching_depth):
                    self.branches[current_branch+index].append(None)
                self.depth.append(0)
            self.depth[current_branch+index] += 1
            self.branches[current_branch+index].append(next_items[index])
            self.read_program(next_items[index], current_branch+index)

    def print_program(self):
        for branch in self.branches:
            for instruction in branch:
                print(instruction, end='-->')
            print()

    def save_program(self):
        if len(self.branches[0]) == 1:
            self.read_program(self.start_item, 0)

    def execute_program(self):
        if len(self.branches[0]) == 1:
            self.read_program(self.start_item, 0)

    def generate_code(self):
        self.generate_block_code(0)

    def generate_block_code(self, branch):
        if len(self.branches[0]) == 1:
            self.read_program(self.start_item, 0)
        self.python_code += constants.BLOCK_DELIMITOR_START
        for instruction in self.branches[branch]:
            # print('\t'*(branch+1)+f"{instruction}")
            if instruction is None:
                continue
            format = instruction.item_type.format
            code = format
            arguments = instruction.arguments
            for command, variable in arguments.items():
                code = code.replace(f"[{command}]", f"{variable}")
            # print('\t'*(branch+2)+f'{code}')
            self.python_code += branch*constants.INDENT + code
            self.python_code += '\n'
            self.python_code += constants.BLOCK_DELIMITOR_END
            if instruction.item_type.number_outputs > 1:
                self.generate_block_code(branch+1)


















