#!/usr/bin/python

import gi
import sys
import flowchart_item
from typing import List
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw


class ProgramWindow(Gtk.ApplicationWindow):
    def __init__(self, program: flowchart_item.Program,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.program_code = program.python_code
        self.buffer = Gtk.TextBuffer(text=self.program_code)
        self.text_view = Gtk.TextView.new_with_buffer(self.buffer)
        self.set_title("Program")
        self.set_child(self.text_view)


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, type_list: list=[] ,*args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title("Flow Chart App")
        self.maximize()
        self.types = type_list
        self.items = {}
        self.item_rows = {}
        self.argument_buffers = {}
        self.items_from_drop_down_menu = {}
        self.setup_boxes()
        self.create_type_buttons()
        self.setup_image_area()
        self.setup_item_list()
        self.setup_dynamic_config()
        self.setup_header()

    def setup_boxes(self):
        self.main_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.types_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.image_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.config_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.dynamic_config_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.item_list_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.set_child(self.main_box)
        self.main_box.append(self.types_box)
        self.main_box.append(self.image_box)
        self.main_box.append(self.config_box)
        self.config_box.append(self.dynamic_config_box)
        self.config_box.append(self.item_list_box)
        self.config_box.set_homogeneous(True)

    def setup_dynamic_config(self):
        self.config_grid = Gtk.Grid()
        self.config_grid.set_row_spacing(10)
        self.apply_button = Gtk.Button(label="Apply")
        self.apply_button.connect("clicked", self.on_click_apply_button)
        self.remove_button = Gtk.Button(label="Remove")
        self.remove_button.connect("clicked", self.on_click_remove_button)
        self.apply_remove_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.apply_remove_box.append(self.remove_button)
        self.apply_remove_box.append(self.apply_button)
        self.apply_remove_box.set_homogeneous(True)
        self.apply_remove_box.set_spacing(40)
        self.config_grid.attach(self.apply_remove_box, 0, 0, 6, 1)

        self.name_question_label = Gtk.Label(label="Name: ")
        self.name_buffer = Gtk.EntryBuffer(text="")
        self.name_entry = Gtk.Entry(buffer=self.name_buffer)
        self.config_grid.attach(self.name_question_label, 0, 2, 1, 1)
        self.config_grid.attach(self.name_entry, 1, 2, 5, 1)
        
        self.type_question_label = Gtk.Label(label="Type: ")
        self.type_label = Gtk.Label(label="")
        self.config_grid.attach(self.type_question_label, 0, 4, 1, 1)
        self.config_grid.attach(self.type_label, 1, 4, 5, 1)

        self.variables_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, vexpand=True)
        self.scrollable_box = Gtk.ScrolledWindow(child=self.variables_box)
        self.variables_label = Gtk.Label(label="Variables required for item")
        self.config_grid.attach(self.variables_label, 0, 6, 6, 1)
        self.config_grid.attach(self.scrollable_box, 0, 7, 6, 3)

        self.scrollable_next_box = Gtk.ScrolledWindow()
        self.next_label = Gtk.Label(label="Next")
        self.config_grid.attach(self.next_label, 0, 8, 1, 1)
        self.config_grid.attach(self.scrollable_next_box, 0, 9, 6, 3)

        self.dynamic_config_box.append(self.config_grid)

    def setup_header(self):
        self.header = Gtk.HeaderBar()
        self.generate_button = Gtk.Button(label="Generate")
        self.generate_button.connect('clicked', self.on_click_generate_button)
        self.header.pack_end(self.generate_button)
        self.set_titlebar(self.header)

    def on_click_generate_button(self, button):
        program = flowchart_item.Program("program", self.start)
        program.generate_code()
        program_window = ProgramWindow(program)
        program_window.present()

    def create_type_buttons(self):
        self.types_list = []
        self.type_buttons = []
        for flow_type in self.types:
            button = Gtk.Button(label=flow_type.action_type)
            self.type_buttons.append(button)
            self.type_buttons[-1].connect('clicked', self.on_click_type_button)
            self.types_box.append(self.type_buttons[-1])

    def on_click_type_button(self, button):
        item_index = self.type_buttons.index(button)
        flow_type = self.types[item_index]
        new_item = flow_type.create_item()
        self.add_item_list(new_item)
        self.items[new_item.name] = new_item
        if flow_type.number_inputs == 0:
            self.start = new_item
        self.open_config(new_item)

    def open_config(self, item: flowchart_item.FlowChartItem):
        self.item_selected = item
        self.name_buffer.set_text(item.name, len(item.name))
        self.variables_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, vexpand=True)
        self.type_label.set_text(item.item_type.action_type)
        self.branches_dropdown_menus = []
        self.next_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        for variable, current_argument in item.arguments.items():
            argument_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            variable_label = Gtk.Label(label=variable)
            argument_box.append(variable_label)
            current_argument_buffer = Gtk.EntryBuffer()
            if current_argument is not None:
                current_argument_buffer.set_text(current_argument, len(current_argument))
            self.argument_buffers[variable] = current_argument_buffer
            current_argument_entry = Gtk.Entry(buffer=self.argument_buffers[variable])
            argument_box.append(current_argument_entry)
            argument_box.set_spacing(25)
            self.variables_box.append(argument_box)
        for num in range(item.item_type.number_outputs):
            branch_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            branch_label = Gtk.Label(label=f"Branch{num}")
            branch_box.append(branch_label)
            self.find_items_to_be_added()
            branch_drop_down_menu = Gtk.DropDown.new_from_strings(self.items_from_drop_down_menu[self.item_selected.name])
            branch_drop_down_menu.set_enable_search(True)
            self.branches_dropdown_menus.append(branch_drop_down_menu)
            branch_box.append(branch_drop_down_menu)
            self.next_box.append(branch_box)
        self.scrollable_next_box.set_child(self.next_box)
        self.scrollable_box.set_child(self.variables_box)

    def on_click_apply_button(self, button):
        if self.item_selected is None:
            return
        item = self.item_selected
        new_name = self.name_entry.get_text()
        arguments = {key: value.get_text() for key, value in self.argument_buffers.items()}
        self.item_list.remove(self.item_rows.pop(self.item_selected.name))
        item.name = new_name
        item.arguments = arguments
        self.items[new_name] = item
        self.add_item_list(self.items[new_name])
        self.item_selected = None
        for num, branch in enumerate(self.branches_dropdown_menus):
            selected = branch.get_selected()
            print(selected)
            if selected == Gtk.INVALID_LIST_POSITION:
                print("Next Not returned")
                break
            else:
                
                item_name_selected = self.items_from_drop_down_menu[item.name][num]
                item_selected = self.items[item_name_selected]
                item.add_to_program_queue(item_selected)
        self.branches_dropdown_menus = []


    def on_click_remove_button(self, button):
        if self.item_selected is None:
            return
        if self.confirm_removal():
            self.item_list.remove(self.item_rows.pop(self.item_selected.name))
            self.items.pop(self.item_selected.name)
            self.item_selected.remove_program_from_queue()
            self.item_selected = None
            self.name_buffer.set_text("", 0)
            self.type_label.set_text("")
            self.variables_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, vexpand=True)
            self.scrollable_box.set_child(self.variables_box)
            self.branches_dropdown_menus = []

    def confirm_removal(self):
        # dialog = CustomDialog(self, self.item_selected)
        # response = dialog.present()
        # if response == Gtk.ResponseType.YES:
        #     dialog.close()
        #     return True
        # return False
        return True

    def setup_item_list(self):
        self.item_list_scroll = Gtk.ScrolledWindow()
        self.item_list_scroll.set_vexpand(True)
        self.item_list = Gtk.ListBox()
        header_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        header_box.set_homogeneous(True)
        name_header_label = Gtk.Label(label="Name")
        type_header_label = Gtk.Label(label="Type")
        header_box.append(name_header_label)
        header_box.append(type_header_label)
        header_box.set_spacing(25)
        header = Gtk.ListBoxRow()
        header.set_selectable(False)
        header.set_child(header_box)
        self.item_list.append(header)
        self.item_list.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.item_list_scroll.set_min_content_width(150)
        self.item_list_scroll.set_child(self.item_list)
        self.item_list_box.append(self.item_list_scroll)
        self.item_list_box.set_vexpand(True)

    def add_item_list(self, item: flowchart_item.FlowChartItem):
        item_row = Gtk.ListBoxRow()
        item_grid = Gtk.Grid()
        item_name_label = Gtk.Label(label=item.name)
        item_type_label = Gtk.Label(label=item.item_type.action_type)
        item_grid.set_column_spacing(25)
        item_grid.attach(item_name_label,0,0,1,1)
        item_grid.attach(item_type_label,1,0,1,1)
        item_row.set_child(item_grid)
        self.item_rows[item.name] = item_row
        self.item_list.connect('row-activated', self.on_row_select)
        self.item_list.append(item_row)

    def setup_image_area(self):
        self.image_area = Gtk.DrawingArea()
        self.image_area.set_hexpand(True)
        self.image_area.set_vexpand(True)
        self.image_box.append(self.image_area)
        self.image_area.set_draw_func(self.draw)

    def draw(self, drawing_area, cairo_context, width, height):
        pass

    def on_row_select(self, list_box: Gtk.ListBox, row: Gtk.ListBoxRow):
        item_grid = row.get_child() 
        item_label = item_grid.get_child_at(0,0)
        item_name = item_label.get_label()
        self.open_config(self.items[item_name])

    def find_items_to_be_added(self):
        items_to_be_added: List[str] = []
        assert self.item_selected is not None
        for item_name, item in self.items.items():
            if item is self.item_selected:
                continue
            if len(item.previous) < item.item_type.number_inputs:
                items_to_be_added.append(item_name)
        self.items_from_drop_down_menu[self.item_selected.name] = items_to_be_added
        

class Application(Adw.Application):
    def __init__(self, types: list, **kwargs):
        super().__init__(**kwargs)
        self.connect('activate', self.on_activate)
        self.types = types

    def on_activate(self, app):
        window = MainWindow(self.types, application=app)
        window.present()
        self.previous_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)


if __name__ == "__main__":
    pass
