import io


class DataStorage:
    def __init__(self, filename: str="data", keys: list=[]):
        self.filename = filename
        self.keys = keys

    def store_data(self, data_to_store: dict):
        if not list(data_to_store.keys()) == self.keys:
            raise Exception("Wrong data type for this object")
        keys = data_to_store.keys()
        data_bytes = io.BytesIO()
        key_len = bytearray()
        for key in keys:
            key_len.append(len(key))
            key_bytes = key.encode('ascii')
            data_bytes.write(key_bytes)
        
            

    def load_data(self):
        pass
    
    def read_data(self):
        with open(self.filename, 'rb') as data:
            data.seek(0,0)
            self.header_byte_num = data.read(4)

            
